const fs = require('fs');
const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
})

const createFiles = (name) => {
    if(!fs.existsSync(`src/${name}`)){
        fs.mkdirSync(`src/${name}`)
    }

    fs.writeFile(`src/${name}/index.js`, `const testFunction = () => {
        console.log('Hello World')
        }
            
        module.exports = testFunction;
    `, err => {
        if(err) throw err;
        console.log('File Created')
    })

    fs.writeFile(`src/${name}/index.test.js`, `const testFunction  = require('./index');

    test('Hello World', () => {
        testFunction()
    })
    `, err => {
        if(err) throw err;
        console.log('File Created')
    })
}


const createNewKata = () => {

    readline.question('Name of new Kata ? : ', name => {
        createFiles(name.replace(/\s/g, ''));
        readline.close();
    })
}



createNewKata();